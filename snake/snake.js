(function(){
  if (typeof SnakeJS === "undefined"){
    window.SnakeJS = {};
  }

  var Coord = SnakeJS.Coord = function(x, y) {
    this.x = x;
    this.y = y;
  };

  Coord.prototype.eq = function(otherCoord) {
    return (this.x === otherCoord.x && this.y === otherCoord.y);
  };

  Coord.prototype.plus = function(coord1, coord2) {
    return new Coord(coord1.x + coord2.x, coord1.y + coord2.y);
  };

  Coord.prototype.goNorth = function() {
    return Coord.prototype.plus(this, new Coord(0, -1));
  };

  Coord.prototype.goSouth = function() {
    return Coord.prototype.plus(this, new Coord(0, 1));
  };

  Coord.prototype.goEast = function() {
    return Coord.prototype.plus(this, new Coord(1, 0));
  };

  Coord.prototype.goWest = function() {
    return Coord.prototype.plus(this, new Coord(-1, 0));
  };

  var Snake = SnakeJS.Snake = function(dir){
    this.dir = dir;
    var c1 = new Coord(15,6);
    var c2 = new Coord(15,7);
    var c3 = new Coord(15,8);
    var c4 = new Coord(15,9);
    var c5 = new Coord(15,10);
    var c6 = new Coord(15,11);

    this.segments = [c1,c2,c3,c4,c5,c6];
  };

  Snake.prototype.move = function(){
    this.segments.pop();
    var newHead;
    var currHead = this.segments[0];
    if (this.dir === "N"){
      newHead = currHead.goNorth();
    }
    else if (this.dir === "S") {
      newHead = currHead.goSouth();
    }
    else if (this.dir === "E") {
      newHead = currHead.goEast();
    }
    else {
      newHead = currHead.goWest();
    }
    this.segments = [newHead].concat(this.segments);
  };

  Snake.prototype.turn = function(newdir) {
    this.dir = newdir;
  };

  Snake.prototype.turnRight = function(){
    if (this.dir==="N"){
      this.turn("E");
    }
    else if (this.dir==="E"){
      this.turn("S");
    }
    else if (this.dir==="S"){
      this.turn("W");
    }
    else {
      this.turn("N");
    }
  }
  Snake.prototype.turnLeft = function(){
    if (this.dir==="N"){
      this.turn("W");
    }
    else if (this.dir==="E"){
      this.turn("N");
    }
    else if (this.dir==="S"){
      this.turn("E");
    }
    else {
      this.turn("S");
    }
  }

  Snake.prototype.isAt = function(coord) {
    for (var i = 0; i < this.segments.length; i++) {
      if (this.segments[i].eq(coord)) {
        return true;
      }
    }
    return false;
  };

  var Board = SnakeJS.Board = function(width, height) {
    this.snake = new Snake("N");
    this.apples = [];
    this.width = width;
    this.height = height;
  };

  Board.prototype.render = function() {
    var renderString = "";
    for (var i = 0; i < this.height; i++) {
      var rowString = "";
      for (var j = 0; j < this.width; j++) {
        coord = new SnakeJS.Coord(j, i);
        if (this.snake.isAt(coord)) {
          rowString += "S";
        }
        else {
          rowString += ".";
        }
      }
      rowString += "<br>";
      renderString += rowString;
    }
    return renderString;
  };

})();