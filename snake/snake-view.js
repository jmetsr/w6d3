(function(){
  if (typeof SnakeJS === "undefined") {
    window.SnakeJS = {};
  }

  var View = SnakeJS.View = function($el) {
    this.$el = $el;
    this.board = new SnakeJS.Board(50, 50);
    var that = this;
    $(document).on("keydown",function(event) {
      if (event.which === 39) {
        that.board.snake.turnRight();
      }
      else if (event.which === 37) {
        that.board.snake.turnLeft();
      }
    });
    window.setInterval(function() {
      that.step();
    }, 500);
  };

  View.prototype.step = function() {
    this.board.snake.move();
    this.$el.text(this.board.render());
  };
})();